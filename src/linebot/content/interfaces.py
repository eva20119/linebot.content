# -*- coding: utf-8 -*-
"""Module where all interfaces, events and exceptions live."""

from linebot.content import _
from zope import schema
from zope.interface import Interface
from zope.publisher.interfaces.browser import IDefaultBrowserLayer
from z3c.relationfield.schema import RelationList, RelationChoice
from plone.app.vocabularies.catalog import CatalogSource

class ILinebotContentLayer(IDefaultBrowserLayer):
    """Marker interface that defines a browser layer."""

class IOrder(Interface):
    title = schema.TextLine(
        title=_(u'Title'),
        required=False
    )
    order_list = RelationList(
        title=_(u"order"),
        value_type=RelationChoice(title=_(u"Choice Order"),
                                  source=CatalogSource(Type='Order'),),
        required=False,
    )