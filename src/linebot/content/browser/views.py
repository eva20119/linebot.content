# -*- coding: utf-8 -*- 
import sys
reload(sys)
sys.setdefaultencoding('utf-8')
from Products.Five.browser import BrowserView
from Products.Five.browser.pagetemplatefile import ViewPageTemplateFile
from plone import api
import requests
import re
import random
from bs4 import BeautifulSoup
import json
import errno
import os
from DateTime import DateTime
import hashlib
import urllib
from Products.CMFPlone.utils import safe_unicode

class GetProfile(BrowserView):
    def __call__(self):
        msg = self.request.get('msg')
        user_id = self.request.get('user_id')
        value = []

        brain = api.content.find(portal_type='Order')
        for item in brain:
            for subject in  item.getObject().Subject():
                if subject == msg:
                    order_list = item.getObject().order_list
                    if len(order_list) >=2:
                        for order in order_list:
                            title = order.to_object.title
                            value.append(title)
                            reply_type = 'button_template'
                    else:
                        reply_type = 'text'
                        value = order_list[0].to_object.title
        
        if value == []:
            order_list = self.context.order_list
            for order in order_list:
                value.append(order.to_object.title)
                reply_type = 'button_template'
        data = {
            'type':reply_type,
            'value':value
        }
        return json.dumps(data)
