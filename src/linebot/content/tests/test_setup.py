# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from plone import api
from linebot.content.testing import LINEBOT_CONTENT_INTEGRATION_TESTING  # noqa

import unittest


class TestSetup(unittest.TestCase):
    """Test that linebot.content is properly installed."""

    layer = LINEBOT_CONTENT_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if linebot.content is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'linebot.content'))

    def test_browserlayer(self):
        """Test that ILinebotContentLayer is registered."""
        from linebot.content.interfaces import (
            ILinebotContentLayer)
        from plone.browserlayer import utils
        self.assertIn(ILinebotContentLayer, utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = LINEBOT_CONTENT_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        self.installer.uninstallProducts(['linebot.content'])

    def test_product_uninstalled(self):
        """Test if linebot.content is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'linebot.content'))

    def test_browserlayer_removed(self):
        """Test that ILinebotContentLayer is removed."""
        from linebot.content.interfaces import \
            ILinebotContentLayer
        from plone.browserlayer import utils
        self.assertNotIn(ILinebotContentLayer, utils.registered_layers())
